#pragma once

const int UPDATE_RATE = 60;
const int UPDATE_DELTA = 1000000 / UPDATE_RATE;

const char STATE_MANAGER_KEY[] = "state_manager";
const char INPUT_MANAGER_KEY[] = "input_manager";
